build:
	npm run build
	npm run frontend:build
	docker build . -t dev2alert/chat
run:
	docker run -it -p 80:80 --rm --name chat dev2alert/chat
push:
	docker push dev2alert/chat
pull:
	docker pull dev2alert/chat