import * as React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";

export const Home = React.lazy(() => import("./pages/Home"));

export const SupportPanel = React.lazy(() => import("./pages/SupportPanel"));

export function App(): React.ReactElement {
    return <BrowserRouter>
        <React.Suspense fallback={<></>}>
            <Routes>
                <Route index element={<Home />} />
                <Route path="/spanel" element={<SupportPanel />} />
            </Routes>
        </React.Suspense>
    </BrowserRouter>;
}