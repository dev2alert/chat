import * as React from "react";
import styles from "./style.scss";

export interface LoginProps {
    error?: string;
    onLogin?: (login: string, password: string) => any;
}

export class Login extends React.Component<LoginProps> {
    public readonly loginRef: React.RefObject<HTMLInputElement> = React.createRef();
    public readonly passwordRef: React.RefObject<HTMLInputElement> = React.createRef();

    public handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        const login: string = this.loginRef.current?.value ?? "";
        const password: string = this.passwordRef.current?.value ?? "";
        this.props.onLogin?.(login, password);
    }

    public render(): React.ReactElement {
        const {error = null} = this.props;
        return <main className={styles.main}>
            <section className={[styles.section, styles.center].join(" ")}>
                <form className={[styles.form, styles.block].join(" ")} onSubmit={this.handleSubmit.bind(this)}>
                    <h1 className={styles.title}>Вход в панель технической поддержки</h1>
                    <input ref={this.loginRef} className={styles["text-input"]} name="login" placeholder="Логин" />
                    <input ref={this.passwordRef} className={styles["text-input"]} name="password" type="password" placeholder="Пароль" />
                    <button className={styles.button}>Продолжить</button>
                    {error !== null ? <output className={styles.output}>
                        <div className={[styles.alert, styles.error].join(" ")}>
                            {error}
                        </div>
                    </output> : null}
                </form>
            </section>
        </main>;
    }
}