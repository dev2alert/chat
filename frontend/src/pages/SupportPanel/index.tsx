import * as React from "react";
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import {action, makeObservable, observable} from "mobx";
import {observer} from "mobx-react";
import {io, Socket} from "socket.io-client";
import {Login} from "./Login";
import {IDialog, IClient, Panel} from "./Panel";
import styles from "./style.scss";

@observer
export default class SupportPanel extends React.Component {
    public error: string | null = null;
    public authed: boolean = false;
    public socket: Socket = io({autoConnect: false});
    public dialogs: Record<number, IDialog> = {};
    public readonly panelRef: React.RefObject<Panel> = React.createRef();

    constructor(props: {}) {
        super(props);
        makeObservable(this, {
            error: observable,
            handleLogin: action,
            handleLogout: action,
            authed: observable,
            dialogs: observable
        });
    }

    public connect(): void {
        this.socket.connect();
    }

    public handleLogin(login: string, password: string): void {
        if(login === "") {
            this.error = "Введите логин."; 
            return;
        }
        if(password === "") {
            this.error = "Введите пароль."; 
            return;
        }
        if(login !== "supporter" || password !== "qwerty123") {
            this.error = "Неверный логин или пароль.";
            return;
        }
        this.error = null;
        this.authed = true;
    }

    public handleMessage(content: string, client: IClient, my: boolean = false): void {
        const panel: Panel | null = this.panelRef.current;
        if(!panel)
            return;
        let dialog: IDialog | null = this.dialogs[client.id] ?? null;
        if(dialog)
            dialog.messages.push({
                name: !my ? client.name : undefined,
                content,
                dateCreated: new Date,
                unread: true,
                my
            });
        else this.dialogs[client.id] = {
            client,
            messages: observable([
                {
                    name: !my ? client.name : undefined,
                    content,
                    dateCreated: new Date,
                    unread: true,
                    my
                }
            ])
        };
        dialog = this.dialogs[client.id];
        if(dialog === panel.getOpenedDialog())
            panel.readMessages(dialog);
        panel.scrollBottom();
    }

    public handleDialogSend(message: string, dialog: IDialog): void {
        this.socket.emit("support-message", message, dialog.client);
    }

    public handleLogout(): void {
        this.authed = false;
    }

    public render(): React.ReactElement {
        return <div className={styles["support-panel"]}>
            <Helmet>
                <title>Панель технической поддержки</title>
            </Helmet>
            <header className={styles.header}>
                <Link to="/">
                    <span className={styles.logo} />
                </Link>
            </header>
            {!this.authed ? <Login 
                error={this.error ?? undefined}
                onLogin={this.handleLogin.bind(this)} 
            /> : <Panel 
                ref={this.panelRef}
                supporter={{
                    photoUrl: "/images/supporter-photo-default.png",
                    name: "Артем"
                }}
                dialogs={Object.values(this.dialogs)}
                onLogout={this.handleLogout.bind(this)}
                onDialogSend={this.handleDialogSend.bind(this)}
            />}
        </div>;
    }

    public componentDidMount(): void {
        this.connect();
        this.socket.on("connect", () => this.socket.emit("support-join"));
        this.socket.on("message", this.handleMessage.bind(this));
    }
}