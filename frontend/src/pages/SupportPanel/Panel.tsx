import * as React from "react";
import {makeObservable, observable, action} from "mobx";
import {observer} from "mobx-react";
import {PanelNavLinks} from "./PanelNavLinks";
import styles from "./style.scss";

export interface ISupporter {
    photoUrl: string;
    name: string;
}

export interface IClient {
    id: number;
    photoUrl: string;
    name: string;
}

export interface IMessage {
    name?: string;
    content: string;
    dateCreated: Date;
    unread: boolean;
    my?: boolean;
}

export interface IDialog {
    client: IClient;
    messages: IMessage[];
}

export interface PanelProps {
    supporter: ISupporter;
    dialogs: IDialog[];
    onLogout?: () => any;
    onDialogSend?: (message: string, dialog: IDialog) => any;
}

@observer
export class Panel extends React.Component<PanelProps> {
    public openedDialogIndex: number = 0;
    public readonly inputRef: React.RefObject<HTMLInputElement> = React.createRef();
    public readonly messagesRef: React.RefObject<HTMLDivElement> = React.createRef();

    constructor(props: PanelProps) {
        super(props);
        makeObservable(this, {
            openedDialogIndex: observable,
            handleDialogOpen: action,
            readMessages: action
        });
    }

    public clearInput(): void {
        if(!this.inputRef.current)
            return;
        this.inputRef.current.value = "";
    }

    public scrollBottom(): void {
        if(!this.messagesRef.current)
            return;
        this.messagesRef.current.scrollTop = this.messagesRef.current.scrollHeight;
    }

    public getUnreadMessagesCount(dialog: IDialog): number {
        let count: number = 0;
        for(const message of dialog.messages) {
            if(message.unread)
                count++;
        }
        return count;
    }

    public readMessages(dialog: IDialog): void {
        for(const message of dialog.messages)
            message.unread = false;
    }

    public handleDialogOpen(index: number): void {
        this.openedDialogIndex = index;
        this.readMessages(this.props.dialogs[this.openedDialogIndex]);
        this.scrollBottom();
    }

    public getOpenedDialog(): IDialog {
        return this.props.dialogs[this.openedDialogIndex];
    }

    public handleDialogSend(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        const message: string = this.inputRef.current?.value ?? "";
        if(message === "")
            return;
        const {dialogs} = this.props;
        const openedDialog: IDialog = dialogs[this.openedDialogIndex] ?? dialogs[0];
        this.props.onDialogSend?.(message, openedDialog);
        this.clearInput();
        this.scrollBottom();
    }

    public render(): React.ReactElement {
        const {supporter, dialogs, onLogout} = this.props;
        const openedDialog: IDialog = dialogs[this.openedDialogIndex] ?? dialogs[0];
        return <main className={[styles.main, styles.panel].join(" ")}>
            <section className={[styles.section, styles.main].join(" ")}>
                <div className={styles.container}>
                    <div className={[styles.block, styles.nav].join(" ")}>
                        <div className={styles.supporter}>
                            <div className={styles.left}>
                                <img className={styles.photo} src={supporter.photoUrl} />
                                <div className={styles.right}>
                                    <span className={styles.name}>{supporter.name}</span>
                                    <span className={styles.role}>Служба поддержки</span>
                                </div>
                            </div>
                            <button className={styles.logout} onClick={onLogout} />
                        </div>
                        <PanelNavLinks />
                    </div>
                    <div className={styles.dialogs}>
                        <h3 className={styles.title}>Диалоги</h3>
                        {dialogs.length === 0 ? <div className={[styles.block, styles.empty].join(" ")}>
                            <div className={styles.image} />
                            <h1 className={styles.title}>Список сообщений пуст</h1>
                        </div> : <div className={[styles.block, styles.list].join(" ")}>
                            <div className={styles.nav}>
                                {dialogs.map((dialog, index) => {
                                    const {client, messages} = dialog;
                                    const classList: string[] = [styles.dialog];
                                    if(dialog === openedDialog)
                                        classList.push(styles.active);
                                    const lastMessage: IMessage = messages[messages.length - 1];
                                    const unreadMessagesCount: number = this.getUnreadMessagesCount(dialog);
                                    return <div className={classList.join(" ")} key={index} onClick={this.handleDialogOpen.bind(this, index)}>
                                        <div className={styles.left}>
                                            <img className={styles.photo} src={client.photoUrl} />
                                            <div className={styles.right}>
                                                <div className={styles.name}>{client.name}</div>
                                                <div className={styles.message}>{lastMessage.content}</div>
                                            </div>
                                        </div>
                                        <div className={styles.right}>
                                            <time className={styles.time}>{lastMessage.dateCreated.toLocaleTimeString(undefined, {hour: "2-digit", minute: "2-digit"})}</time>
                                            {unreadMessagesCount !== 0 ? <div className={styles.count}>{unreadMessagesCount}</div> : null}
                                        </div>
                                    </div>;
                                }).reverse()}
                            </div>
                            <div className={styles.chat}>
                                <div className={styles.top}>
                                    <div className={styles.user}>
                                        <img className={styles.photo} src={openedDialog.client.photoUrl} />
                                        <span className={styles.name}>{openedDialog.client.name}</span>                                   
                                    </div>
                                </div>
                                <div className={styles.messages} ref={this.messagesRef}>
                                    <div className={styles.list}>
                                        {openedDialog.messages.map(({my = false, name = null, content, dateCreated}, index) => {
                                            const classList: string[] = [styles.item];
                                            if(my)
                                                classList.push(styles.my);
                                            return <div className={classList.join(" ")} key={index}>
                                                <div className={styles.container}>
                                                    {name ? <div className={styles.name}>{name}</div> : null}
                                                    <div className={styles.content}>
                                                        <span className={styles.text}>{content}</span>
                                                        <time className={styles.time}>{dateCreated.toLocaleTimeString(undefined, {hour: "2-digit", minute: "2-digit"})}</time>
                                                    </div>
                                                </div>
                                            </div>;
                                        })}
                                    </div>
                                </div>
                                <form className={styles.input} onSubmit={this.handleDialogSend.bind(this)}>
                                    <input 
                                        className={styles.content} 
                                        placeholder="Введите сообщение"
                                        ref={this.inputRef}
                                    />
                                    <button className={styles.send} />
                                </form>
                            </div>
                        </div>}
                    </div>
                </div>
            </section>
        </main>;
    }

    public componentDidMount(): void {
        this.scrollBottom();
    }
}