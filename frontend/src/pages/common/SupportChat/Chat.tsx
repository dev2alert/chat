import * as React from "react";
import {observer} from "mobx-react";
import styles from "./style.scss";

export interface ISupporter {
    name: string;
    photoUrl: string;
}

export interface IMessage {
    content: string;
    dateCreated: Date;
    name?: string;
    my?: boolean;
}

export interface ChatProps {
    supporter?: ISupporter;
    messages?: IMessage[];
    name?: string;
    onClose?: () => any;
    onSend?: (message: string) => any;
    onNameChange?: (name: string) => any;
}

@observer
export class Chat extends React.Component<ChatProps> {
    public readonly inputRef: React.RefObject<HTMLInputElement> = React.createRef();
    public readonly messagesRef: React.RefObject<HTMLDivElement> = React.createRef();
    public readonly nameInputRef: React.RefObject<HTMLInputElement> = React.createRef();

    public clearInput(): void {
        if(!this.inputRef.current)
            return;
        this.inputRef.current.value = "";
    }

    public scrollBottom(): void {
        if(!this.messagesRef.current)
            return;
        this.messagesRef.current.scrollTop = this.messagesRef.current.scrollHeight;
    }

    public handleClose(): void {
        this.props.onClose?.();
    }

    public handleSend(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        const message: string = this.inputRef.current?.value ?? "";
        if(message === "")
            return;
        this.props.onSend?.(message);
        this.clearInput();
        this.scrollBottom();
    }

    public handleNameSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        const name: string = this.nameInputRef.current?.value ?? "";
        if(name === "")
            return;
        this.props.onNameChange?.(name);
    }

    public render(): React.ReactElement {
        const {supporter = null, messages = [], name = null} = this.props;
        return <div className={styles.chat}>
            <div className={styles.head}>
                {supporter ? <div className={styles.supporter}>
                    <img className={styles.photo} src={supporter.photoUrl} />
                    <div className={styles.right}>
                        <h3 className={styles.name}>{supporter.name}</h3>
                        <h4 className={styles.role}>Служба поддержки</h4>
                    </div>
                </div> : <div className={styles.message}>
                    <h1 className={styles.text}>Напишите ваше сообщение</h1>
                    <h2 className={[styles.text, styles.second].join(" ")}>Операторы онлайн!</h2>
                </div>}
                <button className={styles.close} onClick={this.handleClose.bind(this)} />
            </div>
            <div className={styles.body}>
                {name === null ? <form className={styles.form} onSubmit={this.handleNameSubmit.bind(this)}>
                    <h1 className={[styles.title, styles.center].join(" ")}>Представьтесь!</h1>
                    <input 
                        className={[styles.input, styles.center].join(" ")} 
                        placeholder="Введите свое имя" 
                        name="name"
                        ref={this.nameInputRef}
                    />
                    <button className={styles.button}>Продолжить</button>
                </form> : <>
                    <div className={styles.messages} ref={this.messagesRef}>
                        <div className={styles.list}>
                            {messages.map(({content, dateCreated, name = null, my = false}, index) => {
                                const classList: string[] = [styles.item];
                                if(my)
                                    classList.push(styles.my);
                                return <div className={classList.join(" ")} key={index}>
                                    <div className={styles.container}>
                                        {name ? <div className={styles.name}>{name}</div> : null}
                                        <div className={styles.content}>
                                            <span className={styles.text}>{content}</span>
                                            <time className={styles.time}>{dateCreated.toLocaleTimeString(undefined, {hour: "2-digit", minute: "2-digit"})}</time>
                                        </div>
                                    </div>
                                </div>;
                            })}
                        </div>
                    </div>
                    <form className={styles.input} onClick={this.handleSend.bind(this)}>
                        <input 
                            className={styles.content} 
                            placeholder="Введите сообщение"
                            ref={this.inputRef}
                        />
                        <button className={styles.send} />
                    </form>
                </>}
            </div>
        </div>;
    }

    public componentDidMount(): void {
        this.scrollBottom();
    }
}