import * as React from "react";
import styles from "./style.scss";

export interface OpenProps {
    onOpen?: () => any;
}

export function Open({onOpen}: OpenProps): React.ReactElement {
    return <div className={styles.open} onClick={onOpen}>
        <span className={styles.text}>Напишите нам, мы онлайн!</span>
        <span className={styles.logo} />
    </div>
}