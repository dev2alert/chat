import * as React from "react";
import {action, makeObservable, observable} from "mobx";
import {observer} from "mobx-react";
import {io, Socket} from "socket.io-client";
import {Chat, IMessage, ISupporter} from "./Chat";
import {Open} from "./Open";
import styles from "./style.scss";

@observer
export class SupportChat extends React.Component {
    public opened: boolean = false;
    public name: string | null = null;
    public socket: Socket = io({autoConnect: false});
    public messages: IMessage[] = [];
    public supporter: ISupporter | null = null;
    public chatRef: React.RefObject<Chat> = React.createRef();

    constructor(props: {}) {
        super(props);
        makeObservable(this, {
            opened: observable,
            name: observable,
            handleOpen: action,
            handleClose: action,
            handleNameChange: action,
            supporter: observable,
            messages: observable,
            handleMessage: action
        });
    }

    public connect(): void {
        this.socket.connect();
    }

    public disconnect(): void {
        this.socket.disconnect();
    }
    
    public handleOpen(): void {
        this.opened = true;
        this.connect();
    }

    public handleClose(): void {
        this.opened = false;
    }

    public handleSend(message: string): void {
        this.socket.emit("message", message);
    }

    public handleNameChange(name: string): void {
        this.name = name;
        this.socket.emit("name", this.name);
    }

    public handleMessage(content: string, supporter: ISupporter | null = null): void {
        const message: IMessage = {
            name: supporter ? supporter.name : undefined, 
            content, 
            dateCreated: new Date,
            my: supporter === null
        };
        this.messages.push(message);
        if(supporter !== null)
            this.supporter = supporter;
        setTimeout(() => this.chatRef.current?.scrollBottom(), 500);
    }

    public render(): React.ReactElement {
        return <div className={styles["support-chat"]}>
            {this.opened ? 
                <Chat 
                    ref={this.chatRef}
                    name={this.name ?? undefined}
                    supporter={this.supporter ?? undefined}
                    messages={this.messages}
                    onSend={this.handleSend.bind(this)}
                    onClose={this.handleClose.bind(this)} 
                    onNameChange={this.handleNameChange.bind(this)}
                /> : <Open onOpen={this.handleOpen.bind(this)} />}
        </div>;
    }

    public componentDidMount(): void {
        this.socket.on("connect", () => this.socket.emit("client-join"));
        this.socket.on("message", this.handleMessage.bind(this));
    }

    public componentWillUnmount(): void {
        this.disconnect();
    }
}