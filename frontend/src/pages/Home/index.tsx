import * as React from "react";
import {Helmet} from "react-helmet";
import styles from "./style.scss";
import {SupportChat} from "../common/SupportChat";

export default function Home(): React.ReactElement {
    return <>
        <Helmet>
            <title>Home</title>
        </Helmet>
        <header className={styles.header}>
            Header
        </header>
        <main className={styles.main}>
            Main
        </main>
        <footer className={styles.footer}>
            Footer
        </footer>
        <SupportChat />
    </>;
}