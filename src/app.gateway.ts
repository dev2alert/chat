import {SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect} from "@nestjs/websockets";
import {Socket, Server} from "socket.io";

export interface IClient {
    id: number;
    photoUrl: string;
    name: string;
}

export interface IClientData {
    id: number;
    name: string;
}

@WebSocketGateway()
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() 
    public readonly server: Server;

    public readonly clientData: Map<Socket, IClientData> = new Map;
    public lastClientId: number = 0;

    public generateClientId(): number {
        return ++this.lastClientId;
    }

    @SubscribeMessage("client-join")
    public handleClientJoin(socket: Socket): void {
        const id: number = this.generateClientId();
        const data: IClientData = {
            id,
            name: "Аноним"
        };
        this.clientData.set(socket, data);
        socket.join("client#" + id);
    }

    @SubscribeMessage("support-join")
    public handleSupportJoin(socket: Socket): void {
        socket.join("support");
    }
      
    @SubscribeMessage("name")
    public handleNameChange(socket: Socket, name: string): void {
        const data: IClientData | undefined = this.clientData.get(socket);
        if(!data)
            return;
        data.name = name;
    }
    
    @SubscribeMessage("message")
    public handleMessage(socket: Socket, content: string): void {
        const data: IClientData | undefined = this.clientData.get(socket);
        if(!data)
            return;
        socket.emit("message", content);
        this.server.to("support").emit("message", content, {
            id: data.id,
            name: data.name,
            photoUrl: "/images/user-photo-default.png"
        });
    }

    @SubscribeMessage("support-message")
    public handleSupportMessage(socket: Socket, [content, client]: [string, IClient]): void {
        this.server.to("client#" + client.id).emit("message", content, {
            name: "Артем",
            photoUrl: "/images/supporter-photo-default.png"
        });
        this.server.to("support").emit("message", content, client, true);
    }

    public afterInit(server: Server): void {}
    
    public handleConnection(socket: Socket): void {}

    public handleDisconnect(socket: Socket): void {
        this.clientData.delete(socket);
    }
}