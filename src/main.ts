import {NestFactory} from "@nestjs/core";
import {NestExpressApplication} from "@nestjs/platform-express";
import {Request, Response} from "express";
import {join} from "path";
import {AppModule} from "./app.module";

async function bootstrap(): Promise<void> {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    app.useStaticAssets(join(process.cwd(), "./public"));
    app.use((req: Request, res: Response) => res.sendFile(join(process.cwd(), "./public/index.html")));
    app.listen(80);
}
bootstrap();